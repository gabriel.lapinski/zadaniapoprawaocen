using System;

namespace csharp6
{
    class StringInReverseOrderRecursionTask
    {
        static string StringInReverseOrderRecursion(string str)
        {
            return str.Length > 0 ? str[str.Length - 1] + StringInReverseOrderRecursion(str.Substring(0, str.Length - 1)) : str;
        }

        static void Main(string[] args)
        {
            var str1 = "A";
            var str2 = "34 ( 9  9@*";
            var str3 = "eMpIrE";
            var str4 = string.Empty;

            Console.WriteLine(StringInReverseOrderRecursion(str1));
            Console.WriteLine(StringInReverseOrderRecursion(str2));
            Console.WriteLine(StringInReverseOrderRecursion(str3));
            Console.WriteLine(StringInReverseOrderRecursion(str4));
        }
    }
}