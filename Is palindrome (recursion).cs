using System;

namespace csharp7
{
    class IsPalindromeRecursionTask
    {
        static bool IsPalindromeRecursion(string str)
        {
            if (str.Length == 1 || (str.Length == 2 && str[0] == str[1]))
            {
                return true;
            }
            else if (str.Length > 1)
            {
                if (str[0] != str[str.Length - 1])
                {
                    return false;
                }

                return IsPalindromeRecursion(str.Substring(1, str.Length - 2));
            }

            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(IsPalindromeRecursion("aa"));
            Console.WriteLine(IsPalindromeRecursion("dad"));
            Console.WriteLine(IsPalindromeRecursion("apple"));
            Console.WriteLine(IsPalindromeRecursion("deleveled"));
            Console.WriteLine(IsPalindromeRecursion(""));
            Console.WriteLine(IsPalindromeRecursion("hannah"));
            Console.WriteLine(IsPalindromeRecursion("X"));
        }
    }
}