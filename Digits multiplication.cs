using System;

namespace csharp
{
    class DigitsMultiplicationTask
    {
        static int DigitsMultiplication(int num)
        {
            return num > 10 ? num % 10 * DigitsMultiplication(num / 10) : num % 10;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(DigitsMultiplication(1234));
            Console.WriteLine(DigitsMultiplication(94632));
            Console.WriteLine(DigitsMultiplication(222222222));
        }
    }
}