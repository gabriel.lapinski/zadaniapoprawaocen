using System;

namespace csharp4
{
    class NumbersMultiplicationTask
    {
        static int NumbersMultiplication(int from, int to)
        {
            while (from == to)
            {
                return from;
            }

            return from * NumbersMultiplication(from + 1, to);
        }

        static void Main(string[] args)
        {
            Console.WriteLine($"{NumbersMultiplication(1, 5)}");
            Console.WriteLine($"{NumbersMultiplication(-27, -22)}");
            Console.WriteLine($"{NumbersMultiplication(44, 44)}");
        }
    }
}