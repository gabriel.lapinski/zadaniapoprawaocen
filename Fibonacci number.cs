using System;

namespace csharp3
{
    class FibonacciNumberTask
    {
        static int FibonacciNumber(int num)
        {
            return num <= 1 ? num : FibonacciNumber(num - 2) + FibonacciNumber(num - 1);
        }

        static void Main(string[] args)
        {
            Console.WriteLine(FibonacciNumber(10));
            Console.WriteLine(FibonacciNumber(0));
            Console.WriteLine(FibonacciNumber(20));
            Console.WriteLine(FibonacciNumber(13));
        }
    }
}