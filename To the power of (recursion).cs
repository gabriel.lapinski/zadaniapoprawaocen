using System;

namespace csharp5
{
    class ToThePowerOfRecursionTask
    {
        static int ToThePowerOfRecursion(int b, int exp)
        {
            return exp == 0 ? 1 : exp > 1 ? b * ToThePowerOfRecursion(b, exp - 1) : b;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(ToThePowerOfRecursion(10, 0));
            Console.WriteLine(ToThePowerOfRecursion(3, 7));
            Console.WriteLine(ToThePowerOfRecursion(2, 10));
        }
    }
}