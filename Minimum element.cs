using System;

namespace csharp8
{
    class MinimumElementTask
    {
        static int MinimumElement(int[] arr, int size)
        {
            if (size == 1)
            {
                return arr[0];
            }

            return arr[size - 1] < MinimumElement(arr, size - 1) ? arr[size - 1] : MinimumElement(arr, size - 1);
        }

        static void Main(string[] args)
        {
            Console.WriteLine(MinimumElement(new int[] { 7, 2, 9, 5, 4 }, 5));
            Console.WriteLine(MinimumElement(new int[] { -45, -6, 39, 96, -20, 0, -100 }, 7));
            Console.WriteLine(MinimumElement(new int[] { 830, 905, 999, 831, 920, 900 }, 6));
        }
    }
}